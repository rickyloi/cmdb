from django.contrib import admin
from .models import *
from django.utils.text import capfirst
from import_export.admin import ImportExportActionModelAdmin, ImportExportModelAdmin

from django.contrib.auth import get_user_model
Users = get_user_model()

admin.site.register(Users)

@admin.register(Permissions)
class PermissionsAdmin(ImportExportActionModelAdmin, ImportExportModelAdmin):
    all_fields = list(key.name for key in Permissions._meta.fields )  # 变量字段排除id
    list_display = all_fields

@admin.register(Roles)
class RolesAdmin(ImportExportActionModelAdmin, ImportExportModelAdmin):
    all_fields = list(key.name for key in Roles._meta.fields )  # 变量字段排除id 
    list_display = all_fields

admin.site.register(Departments)




# 排序
def find_model_index(name):
    count = 0
    for model, model_admin in admin.site._registry.items():
        if capfirst(model._meta.verbose_name_plural) == name:
            return count
        else:
            count += 1
    return count

def index_decorator(func):
    def inner(*args, **kwargs):
        templateresponse = func(*args, **kwargs)
        for app in templateresponse.context_data['app_list']:
            app['models'].sort(key=lambda x: find_model_index(x['name']))
        return templateresponse
    return inner
