from django.shortcuts import render
from api.serializers.app_application import *

from rest_framework.viewsets import ModelViewSet
from rest_framework.filters import SearchFilter, OrderingFilter
from utils.views import ChoiceAPIView

class ApplicationViewSet(ModelViewSet):
   
    queryset = Application.objects.all()
    serializer_class = ApplicationSerializer
    filter_backends = (SearchFilter, OrderingFilter)
    search_fields = ('name', 'desc') # &search=host-2
    ordering_fields = ('id', 'name')

class ProductViewSet(ModelViewSet):
   
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    filter_backends = (SearchFilter, OrderingFilter)
    search_fields = ('name', 'desc') # &search=host-2
    ordering_fields = ('id', 'name')

class EnvChoicesAPIView(ChoiceAPIView):
    """
    get:
    envconfig--models类型列表

    models中的类型列表信息, status: 200(成功), return: 服务器models中的类型列表
    """
    choice = EnvConfig.env_choices

class EnvPackageChoicesAPIView(ChoiceAPIView):
    """
    get:
    envconfig--models类型列表

    models中的类型列表信息, status: 200(成功), return: 服务器models中的类型列表
    """
    choice = EnvConfig.package_choices

class EnvConfigViewSet(ModelViewSet):
   
    queryset = EnvConfig.objects.all()
    serializer_class = EnvConfigSerializer
    filter_backends = (SearchFilter, OrderingFilter)
    search_fields = ('name', 'desc') # &search=host-2
    ordering_fields = ('id', 'name')


class AppDeveloperViewSet(ModelViewSet):
   
    queryset = AppDeveloper.objects.all()
    serializer_class = AppDeveloperSerializer
    filter_backends = (SearchFilter, OrderingFilter)
    search_fields = ('name', 'desc') # &search=host-2
    ordering_fields = ('id', 'name')

    