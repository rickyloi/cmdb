#!/usr/bin/env python
# coding=utf-8
'''
author: 以谁为师
site: opsbase.cn
Date: 2022-02-11 07:52:27
LastEditTime: 2022-02-11 11:26:19
Description: 
'''

from rest_framework import serializers
from asset.models import *
from .mixins  import *
import json



class SSH_accountSerializer(serializers.ModelSerializer):
    
    gmt_create = serializers.DateTimeField(format='%Y-%m-%d %H:%M:%S', read_only=True)
    gmt_modified = serializers.DateTimeField(format='%Y-%m-%d %H:%M:%S', read_only=True)

    class Meta:
        model = SSH_account
        fields = "__all__"

    def to_representation(self, instance):
        """自定义字段返回 """
        
        data = super().to_representation(instance)
        # 添加choices中文显示
        data['ssh_type_display'] = instance.get_authentication_type_display()
        return data
