from rest_framework import serializers
from asset.models import *
from .mixins  import *
import json

class RegionSerializer(serializers.ModelSerializer):
    
    gmt_create = serializers.DateTimeField(format='%Y-%m-%d %H:%M:%S', read_only=True)
    gmt_modified = serializers.DateTimeField(format='%Y-%m-%d %H:%M:%S', read_only=True)

    class Meta:
        model = Region
        fields = "__all__"